<?php
/**
* This script will reset the password of the selected user and retuurn a randomly generated password
*/


//Getting the config
require_once '../../application/configuration.php';

$error = false;
$data = "";

//The start date of the course
$id = isset($_POST['id']) ? $_POST['id'] : null;


startSession();
if(!validateCurrentUser()){
	$error = true;
	$data = "You must be logged in to do that!";
}

if(!$error){
	$ph = new PasswordHash($GLOBALS['app_vars']['phpass']['log2'], $GLOBALS['app_vars']['phpass']['mobile']);
	$tempPass = generatePassword();
	$tempPassHashed = $ph->HashPassword($tempPass);

	$query = "UPDATE user SET password=? WHERE uuid=?";
	$db = DatabaseFactory::GetInstance()->GetConnection();
	$stmt = $db->prepare($query);

	$stmt->bind_param('ss', $tempPassHashed, $id);
	if($stmt->execute()){
		$data = "User updated!<br><b>Temporary Password: </b> $tempPass";
	}
	else{
		$error = true;
		$data = "There was an issue updating the user! Could not change the password!";
	}
}

echo json_encode(array("error" => $error, "data" => $data));

?>