<?php
	require_once '../application/configuration.php';
	require_once $GLOBALS['app_vars']['header_path'];

	$session_keys = $GLOBALS['app_vars']['session_keys'];
	//Unsetting the user if there is one
	unset($_SESSION[$session_keys['user_name']]);
	unset($_SESSION[$session_keys['user_id']]);
	session_destroy();
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Log In</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="../styles/main.css">
    <link rel="stylesheet" href="style.css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
    <!-- endbuild -->
    <script src="../bower_components/modernizr/modernizr.js"></script>
</head>
<body>



<div class="main-container col-md-12">

    <header>
        <h1>Log In to <b>CallieButton</b></h1>
    </header>

    <aside class="content">

	    <div class='col-md-6 col-lg-4 col-md-offset-3 col-lg-offset-4'>
	    	<div class="table-wrapper col-md-12">
	    	    <div class="table-title">
	    	        <h4>Log In:</h4>
	    	    </div>

	    	    <div class='table-error' id='error-message'>
	    	    	
	    	    </div>

	    	    <div class="table-content">
		    	    <form class='col-md-12' id='login-form' role='form'>
		    	    	<div class='form-group' >
		    	    		<div class='input-group'>
		    	    			<div class='input-group-addon'>@</div>
		    	    			<input class='form-control' id='email' name='email' type='email' placeholder='Enter email' />
		    	    		</div>
		    	    	</div>
		    	    	<div class='form-group'>
		    	    		<div class='input-group'>
		    	    			<div class='input-group-addon'><div class='fa fa-lock'></div></div>
		    	    			<input class='form-control' id='pass' name='pass' type='password' placeholder='Enter password' />
		    	    		</div>
		    	    	</div>
		    	    	<button class='btn btn-success col-md-12' id='submit' style='margin-bottom:10px;'>Submit</button>
		    	    </form>
	    	    </div>
	    	</div>
	    </div>
        
    </aside>

</div> 









<script src="../bower_components/jquery/dist/jquery.js"></script>
<script src="../scripts/pace.js"></script>
<script src="../scripts/main.js"></script>

<script src="../bower_components/bootstrap-sass/assets/javascripts/bootstrap/affix.js"></script>
<script src="../bower_components/bootstrap-sass/assets/javascripts/bootstrap/alert.js"></script>
<script src="../bower_components/bootstrap-sass/assets/javascripts/bootstrap/dropdown.js"></script>
<script src="../bower_components/bootstrap-sass/assets/javascripts/bootstrap/tooltip.js"></script>
<script src="../bower_components/bootstrap-sass/assets/javascripts/bootstrap/modal.js"></script>
<script src="../bower_components/bootstrap-sass/assets/javascripts/bootstrap/transition.js"></script>
<script src="../bower_components/bootstrap-sass/assets/javascripts/bootstrap/button.js"></script>
<script src="../bower_components/bootstrap-sass/assets/javascripts/bootstrap/popover.js"></script>
<script src="../bower_components/bootstrap-sass/assets/javascripts/bootstrap/collapse.js"></script>

<script type="text/javascript">
	$( document ).ready(function(){
		$('#error-message').hide();
	});

	$('#submit').on('click',function(e){
		e.preventDefault();
		var form = $('#login-form').serialize();
		$('#error-message').slideUp();

		$.post('process.php',
			form,
			function(response){
				response = JSON.parse(response);
				if(response.problem){
					$('#error-message').text(response.message);
					$('#error-message').slideDown();
				}
				else{
					window.location = '../';
				}
			});
	});
</script>

</body>
</html>

<?php
	require_once $GLOBALS['app_vars']['footer_path'];
?>