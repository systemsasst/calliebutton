<?php
	function endSession(){
		unset($_SESSION[$GLOBALS['app_vars']['session_keys']['user_name']]);
		unset($_SESSION[$GLOBALS['app_vars']['session_keys']['user_id']]);
		session_destroy();
	}

	function startSession(){
		session_start();
		session_regenerate_id();
	}

	function resetSession(){
		session_start();
		endSession();
		startSession();
	}

	function validateCurrentUser(){
		$email = $_SESSION[$GLOBALS['app_vars']['session_keys']['user_name']];
		$id = $_SESSION[$GLOBALS['app_vars']['session_keys']['user_id']];

		$db = DatabaseFactory::GetInstance()->getConnection();
		$result = false;

		$stmt = $db->prepare('SELECT email FROM user WHERE uuid=?');
		$stmt->bind_param('s', $id);
		$stmt->execute();
		$stmt->store_result();
		if($stmt->num_rows == 1){
			$r_email;
			$stmt->bind_result($r_email);
			$stmt->fetch();
			if($r_email == $email){
				$result = true;
			}
		}


		$stmt->free_result();
		return $result;
	}

	function generatePassword(){
		$pass = "";
		$alpha_pool = "abcdefghjklmnopqrstuvwxyz";
		$number_pool = "0123456789";
		$special_pool = "!@#$-+_";

		for($i = 0; $i < 20; $i ++){
			$rand = rand();
			$num = $i % 4;
			switch ($num) {
				case 3:
					$pass .= $alpha_pool[$rand % strlen($alpha_pool)];
					break;
				case 2:
					$pass .= strtoupper($alpha_pool)[$rand % strlen($alpha_pool)];
					break;
				case 1:
					$pass .= $number_pool[$rand % strlen($number_pool)];
					break;
				default:
					$pass .= $special_pool[$rand % strlen($special_pool)];
					break;
			}
		}

		return $pass;
	}

	function getAllUsers(){
		$users = array();
		$db = DatabaseFactory::GetInstance()->getConnection();

		$query = "SELECT email, first, last FROM user";
		$rs = $db->query($query);

		while($row = $rs->fetch_assoc()){
			$users[] = array('email' => $row['email'], 'first' => $row['first'], 'last' => $row['last']);
		}

		return $users;
	}
?>