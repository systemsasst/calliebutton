<?php
/**
* This script will add a course to the database
*/

//Getting the config
require_once '../../application/configuration.php';

$error = false;
$data = "";

//The start date of the course
$startDate = isset($_POST['startDate']) ? $_POST['startDate'] : null;


startSession();
if(!validateCurrentUser()){
	$error = true;
	$data = "You must be logged in to do that!";
}

if(!$error){
	try{
		if($startDate == null){
			throw new Exception("Please enter a valid start date!");
		}

		$date = new DateTime($startDate);

		$c = new Course(null, $date, true);
		$c->save();

		$data = "Course created!";

	}
	catch(AppException $e){
		$error = true;
		$data = $GLOBALS['app_vars']['debug'] ? $error->getMessage() : $error->getUserMessage();
	}
	catch(Exception $e){
		$error = true;
		$data = $e->getMessage();
	}
}

echo json_encode(array('error' => $error, 'data' => $data));
?>