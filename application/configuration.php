<?php
  /**
  * This file holds all of the configuration options for the application
  * It will also load any class files required
  * Require it at the top of the file
  */


  /***************************
  GLOBAL APPLICATION VARIABLES
  Configure these
  ***************************/
  date_default_timezone_set('America/Los_Angeles');
  //If debugging the application, shows extra information on errors
  $app_vars['debug'] = true;
  //What connection is being used for the database, defined in db_configuration.php
  $app_vars['database_connection'] = "production";
  //Name of the directory where this application is located MUST BE UNIQUE IN THE PATH
  $app_vars['root_dir_name'] = "calliebutton";
  //Base URL for the application
  $app_vars['base_url'] = 'calliebutton';
  //Mailer details
  $app_vars['email_credentials'] = "production";

  if($app_vars['debug']){
    error_reporting(E_ALL); 
    ini_set( 'display_errors','1');
  }




  /***************************
  User Auth Variables
  Configure these
  ***************************/
  $app_vars['min_password_strength'] = 2;
  $app_vars['phpass']['log2'] = 1024;
  $app_vars['phpass']['mobile'] = false;


  /***************************
  Strings
  Configure these
  ***************************/
  $app_vars['strings']['general_error'] = "Sorry, there is a problem with that last request. If this issue persists, please contact the system administrator.";

  /***************************
  Required Components
  Advanced
  ***************************/

  //Root directory on the server for the application
  $app_vars['application_server_root'] = substr(getcwd(), 0, strpos(getcwd(), $app_vars['root_dir_name']) + strlen($app_vars['root_dir_name'])) . '/';
  //The directory for log files
  $app_vars['log_directory'] = $app_vars['application_server_root'] . '/logs';

  //loading the regex's
  require_once $app_vars['application_server_root'] . 'utils/validation/regexes.php';
  $app_vars['regexes'] = $validation_regex;

  require_once 'db_configuration.php';
  $app_vars['database_connection_arguments'] = $dbconfig[$app_vars['database_connection']];

  require_once 'mail_configuration.php';
  $app_vars['email_credentials_arguments'] = $mail_config[$app_vars['email_credentials']];

  require_once $app_vars['application_server_root'] . 'utils/phpass/PasswordHash.php';




  require_once $app_vars['application_server_root'] . '/utils/auth_functions.php';

  require_once $app_vars['application_server_root'] . '/utils/app_functions.php';

  requireFilesInDirectory($app_vars['application_server_root'] . '/classes/', true, 'php');


  /***************************
  Other Variables
  Advanced
  ***************************/
  $app_vars['session_keys']['user_name'] = 'calliebutton_email';
  $app_vars['session_keys']['user_id'] = 'calliebutton_user_id';
  $app_vars['header_path'] = $app_vars['application_server_root'] . 'application/header_footer/header.php';
  $app_vars['footer_path'] = $app_vars['application_server_root'] . 'application/header_footer/footer.php';




  

  //This is the version of app_vars that should be referenced
  $GLOBALS['app_vars'] = $app_vars;

?>
