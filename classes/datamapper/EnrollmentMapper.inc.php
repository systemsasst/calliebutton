<?php

	require_once "EntityMapper.inc.php";

	class EnrollmentMapper extends EntityMapper{
		public function __construct(){
			parent::__construct();
		}

		public function save(Model $enrollment){
			if(get_class($enrollment) != "Enrollment"){
				throw new RequiredModelException("Sorry, something went wrong. If the problem persists, please contact the administrator.","Expected an Enrollment got a " . get_class($enrollment));
			}


			$dbid = $enrollment->getDBID();
			$email = $enrollment->getEmail();
			$name = $enrollment->getName();
			$department = $enrollment->getDepartment();
			$course_name = $enrollment->getCourseName();
			$class_id = $enrollment->getCourseID();
			$start_date = ($enrollment->getStartDate()) ? $enrollment->getStartDate()->format('Y-m-d') : null;
			$end_date = ($enrollment->getEndDate()) ? $enrollment->getEndDate()->format('Y-m-d') : null;
			$reason = $enrollment->getReason();
			$is_new = $enrollment->getIsNew();
			$signed_up_on = ($enrollment->getSignedUpOn()) ? $enrollment->getSignedUpOn()->format('Y-m-d H:i:s') : null;

			//Checking to see if the enrollment is in the database
			$inserting = true;
			$stmt = $this->db->prepare("SELECT uuid FROM enrollment WHERE uuid=?");
			$stmt->bind_param("s",$dbid);
			$stmt->execute();
			$stmt->store_result();
			$count = $stmt->num_rows();

			$inserting = ($count == 0);

			if($inserting){
				$stmt = $this->db->prepare("INSERT INTO enrollment SET uuid=?, course_id=?, email=?, name=?, department=?, course_name=?, start_date=?, end_date=?, reason=?, is_new=?, signed_up_on=?");
				$stmt->bind_param("sssssssssis", $dbid, $class_id, $email, $name, $department, $course_name, $start_date, $end_date, $reason, $is_new, $signed_up_on);
				$stmt->execute();
			}
			else{
				$stmt = $this->db->prepare("UPDATE enrollment SET course_id=?, email=?, name=?, department=?, course_name=?, start_date=?, end_date=?, reason=?, is_new=?, signed_up_on=? WHERE uuid=?");
				$stmt->bind_param("ssssssssiss", $class_id, $email, $name, $department, $course_name, $start_date, $end_date, $reason, $is_new, $signed_up_on, $dbid);
				$stmt->execute();
			}

			$stmt->free_result();
		}

		public function all(){
			$enrollments = array();
			$query = "SELECT uuid, course_id, email, name, department, course_name, start_date, end_date, reason, is_new, signed_up_on FROM enrollment";
			$result = $this->db->query($query);
			
			while($row = $result->fetch_assoc()){

				$startDateString = $row['start_date'];
				$endDateString = $row['end_date'];
				$startDate = ($startDateString != null && $startDateString != 'null') ? new DateTime($startDateString) : null;
				$endDate = ($endDateString != null && $endDateString != 'null') ? new DateTime($endDateString) : null;
				$signed_up_on = ($signed_up_on != null && $signed_up_on != 'null') ? new DateTime($signed_up_on) : null;

				$enrollments[] = new Enrollment($row['uuid'], $row['course_id'], $row['email'], $row['name'], $row['department'], $row['course_name'], $startDate, $endDate, $row['reason'], ($row['is_new'] == 1), $row['signed_up_on']);
			}

			return $enrollments;
		}


		public function find($id){
			$stmt = $this->db->prepare("SELECT uuid, course_id, email, name, department, course_name, start_date, end_date, reason, is_new, signed_up_on FROM enrollment WHERE uuid=?");
			$stmt->bind_param('s', $id);
			$stmt->execute();

			$course_id;
			$email;
			$name;
			$department;
			$course_name;
			$startDateString;
			$endDateString;
			$reason;
			$is_new;
			$signed_up_on;


			$stmt->bind_result($id, $course_id, $email, $name, $department, $course_name, $startDateString, $endDateString, $reason, $is_new, $signed_up_on);
			$stmt->fetch();
			$stmt->free_result();

			$startDate = ($startDateString != null && $startDateString != 'null') ? new DateTime($startDateString) : null;
			$endDate = ($endDateString != null && $endDateString != 'null') ? new DateTime($endDateString) : null;
			$signed_up_on = ($signed_up_on != null && $signed_up_on != 'null') ? new DateTime($signed_up_on) : null;
			return new Enrollment($id, $course_id, $email, $name, $department, $course_name, $startDate, $endDate, $reason, ($is_new == 1), $signed_up_on);
		}

		public function findByCourse($course_id){
			$enrollments = array();
			$stmt = $this->db->prepare("SELECT uuid, course_id, email, name, department, course_name, start_date, end_date, reason, is_new, signed_up_on FROM enrollment WHERE course_id=?");
			$stmt->bind_param('s', $course_id);
			$stmt->execute();

			$course_id;
			$email;
			$name;
			$department;
			$course_name;
			$startDateString;
			$endDateString;
			$reason;
			$is_new;
			$signed_up_on;


			$stmt->bind_result($id, $course_id, $email, $name, $department, $course_name, $startDateString, $endDateString, $reason, $is_new, $signed_up_on);
			while($stmt->fetch()){
				$startDate = ($startDateString != null && $startDateString != 'null') ? new DateTime($startDateString) : null;
				$endDate = ($endDateString != null && $endDateString != 'null') ? new DateTime($endDateString) : null;
				$signed_up_on = ($signed_up_on != null && $signed_up_on != 'null') ? new DateTime($signed_up_on) : null;




				$enrollments[] = new Enrollment($id, $course_id, $email, $name, $department, $course_name, $startDate, $endDate, $reason, ($is_new == 1), $signed_up_on);
			}


			$stmt->free_result();

			return $enrollments;
		}

		public function findByStatus($is_new){
			$enrollments = array();
			$stmt = $this->db->prepare("SELECT uuid, course_id, email, name, department, course_name, start_date, end_date, reason, is_new, signed_up_on FROM enrollment WHERE is_new=?");
			$stmt->bind_param('i', $is_new);
			$stmt->execute();

			$course_id;
			$email;
			$name;
			$department;
			$course_name;
			$startDateString;
			$endDateString;
			$reason;
			$is_new;
			$signed_up_on;

			$stmt->bind_result($id, $course_id, $email, $name, $department, $course_name, $startDateString, $endDateString, $reason, $is_new, $signed_up_on);
			while($stmt->fetch()){
				$startDate = ($startDateString != null && $startDateString != 'null') ? new DateTime($startDateString) : null;
				$endDate = ($endDateString != null && $endDateString != 'null') ? new DateTime($endDateString) : null;
				$signed_up_on = ($signed_up_on != null && $signed_up_on != 'null') ? new DateTime($signed_up_on) : null;

				$enrollments[] = new Enrollment($id, $course_id, $email, $name, $department, $course_name, $startDate, $endDate, $reason, ($is_new == 1), $signed_up_on);
			}


			$stmt->free_result();

			return $enrollments;
		}

		public function delete($id){
			$stmt = $this->db->prepare("DELETE FROM enrollment WHERE uuid=?");
			$stmt->bind_param('s', $id);
			$stmt->execute();
		}


	}

?>