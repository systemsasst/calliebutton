<?php
	require_once '../../application/configuration.php';
	$error = false;
	$data = array();
	$mapper = new CourseMapper();

	$ids = isset($_POST['ids']) ? $_POST['ids'] : null;
	$id = isset($_POST['id']) ? $_POST['id'] : null;
	$getOnlyEnrollable = isset($_POST['enrollable_only']) && $_POST['enrollable_only'] == true;

	startSession();
	if(!validateCurrentUser()){
		$error = true;
		$data = "You must be logged in to do that!";
	}

	if(!$error){
		//pulling multiple courses
		try{
			if($ids != null){
				foreach ($ids as $key => $course_id) {
					$data[] = $mapper->find($course_id)->getModelAsArray();
				}
			}
			//pulling a single course
			else if($id != null){
				$data = $mapper->find($id)->getModelAsArray();
			}
			//pulling all courses
			else{
				if($getOnlyEnrollable){
					$courses = $mapper->allEnrollable();
				}
				$courses = $mapper->all();
				foreach ($courses as $c) {
					$data[] = $c->getModelAsArray();
				}
			}
			
		}
		catch(AppException $e){
			$error = true;
			$data = $GLOBALS['app_vars']['debug'] ? $e->getMessage() : $e->getUserMessage();
		}
		catch(Exception $e){
			$error = true;
			$data = $GLOBALS['app_vars']['debug'] ? "Exception in getCourses! <br>\n" . $e->getMessage() . "<br/>\n" . $e->getTraceAsString(): $GLOBALS['app_vars']['strings']['general_error'];
		}
		
	}

	echo json_encode(array("error" => $error, "data" => $data));
?>