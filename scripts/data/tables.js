//Call this function to fill the data in a table
function fillData($container, model){
	updateData().done(function(){
		$container.html('');
		if(model == 'courses'){
			fillCourses($container);
		}
		else if(model == 'users'){
			fillUsers($container);
		}

		$('.toggle-enrollable').each(function(){
			//alert(this.html());
			$(this).bootstrapSwitch({
			onText: 'Yes',
			offText: 'No',
			onSwitchChange: enrollToggle});
		});
	});
}




function fillCourses($container){
	if(allCourses){
		for(var i = 0; i < allCourses.length; i ++){
			var course = allCourses[i];
			var checked = (course['enrollable']) ? 'checked' : '';
			//alert(course['startDate']);
			var date = moment(course['startDate']);
			var dateString = date.format('MMMM Do, YYYY');

			$container.append("<tr class='course-row' data-dbid='" + course['dbid'] + "'><td class='course-date'>" + dateString + "</td><td>" + course['enrolled'].length + "</td><td><input type='checkbox' " + checked + " name='toggle-enrollable' class='toggle-enrollable'/></td><td><button class='course-info-button btn btn-sm btn-success col-md-12'>More info</button></td><td><button class='btn btn-danger  col-md-12 delete-course'><span class='fa fa-trash'></span></button></td></tr>");
		}
	}
	
}

function fillUsers($container){
	if(allUsers){
		for(var i = 0; i < allUsers.length; i ++){
			var user = allUsers[i];
			var email = user.email;
			var name = user.name;
			var id = user.id;
			var resetHTML = "<button class='btn btn-warning col-md-12 reset-user'>Reset Password</button>"
			var deleteHTML = "<button class='btn btn-danger col-md-12 delete-user'><span class='fa fa-trash'></span></button>";

			$container.append("<tr class='user-row' data-dbid='" + id + "'><td>" + name + "</td><td>" + email + "</td><td>" + resetHTML + "</td><td>" + deleteHTML + "</td></tr>");
		}
	}
}