<?php
/**
* This is HTML that is meant to be included with a page that displays this modal dialog from bootstrap
* This modal will contain the form to add a course.
*/
?>

<div id='add-course-modal' class="modal fade">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add a class</h4>
      </div>
      <div class="modal-body">

      <form>
      	<div class='form-group'>
      		<label for="classs-date">Class Date</label>
      		<input type="date" id='course-date', name='course-date'/>
      	</div>
      </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="add-class btn btn-primary">Add course</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->