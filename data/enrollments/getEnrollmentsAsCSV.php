<?php
/**
* This script will return a csv file to the user containing the enrollments for a certain course
*/


//Getting the config
require_once '../../application/configuration.php';

$error = false;
$data = array();
$courseMapper = new CourseMapper();

//The database id of the course
$id = isset($_POST['id']) ? $_POST['id'] : null;
if($id == null){
	//Trying get if post was empty
	$id = isset($_GET['id']) ? $_GET['id'] : null;
}

$enrollments = array();
$course;

startSession();
if(!validateCurrentUser()){
	$error = true;
	$data = "You must be logged in to do that!";
}

if(!$error){
	if($id != null){
		$course = $courseMapper->find($id);
		if($course == null){
			$error = true;
			$data = "Please select a valid course!";
		}
		else{
			$enrollments = $course->getModelAsArray()['enrolled'];
		}
	}
	else{
		$error = true;
		$data = "You must select a course to do that!";
	}
}

if(!$error){
	//If the course is valid
	//Removing the dbid and the course_id from the table
	for($i = 0; $i < count($enrollments); $i++){
		array_shift($enrollments[$i]);
		array_shift($enrollments[$i]);
	}

	//Adding the labels
	array_unshift($enrollments, array('email','name','department','course_name','start_date','reason', 'is new', 'signed up'));

	$filename = "enrollments_for_" . $course->getStartDate()->format('m-d-Y') . ".csv";

	//header lines for the file
	header("Content-type: application/csv");
	header("Content-Disposition: attachment; filename=$filename");


	$fp = fopen('php://output', 'w');

	for($i = 0; $i < count($enrollments); $i++){
		fputcsv($fp, $enrollments[$i]);
	}

	fclose($fp);
}
else{
	echo $data;
}


?>

