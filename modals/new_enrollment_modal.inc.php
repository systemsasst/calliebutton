<?php
/**
* This is HTML that is meant to be included with a page that displays this modal dialog from bootstrap
* This modal will contain the form to add a course.
*/
?>

<div id='new-enrollment-modal' class="modal fade" data-cur-enrollment-dbid=''>
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">New Enrollments (<span class='number-new-enrollments'></span>)</h4>
      </div>
      <div class="modal-body">

          <div class='panel panel-default'>
            <div class='panel-body'>
              <a class='address' href="mailto:">Email</a>
            </div>
          </div>

          <div class='panel panel-primary'>
            <div class='panel-heading'>Message</div>
            <div class='panel-body message-body'></div>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="next-new-enrollment btn btn-primary">Next Enrollment</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->