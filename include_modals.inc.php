<?php
	require_once $GLOBALS['app_vars']['application_server_root'] . '/modals/course_info_modal.inc.php';
	require_once $GLOBALS['app_vars']['application_server_root'] . '/modals/add_course_modal.inc.php';
	require_once $GLOBALS['app_vars']['application_server_root'] . '/modals/delete_course_modal.inc.php';
	require_once $GLOBALS['app_vars']['application_server_root'] . '/modals/add_user_modal.inc.php';
	require_once $GLOBALS['app_vars']['application_server_root'] . '/modals/new_enrollment_modal.inc.php';
?>