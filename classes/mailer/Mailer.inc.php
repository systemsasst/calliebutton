<?php
/**
* This is a mailer class that built to send emails to the faculty and admins on this site
*/
require_once $GLOBALS['app_vars']['application_server_root'] . '/utils/PHPMailer/PHPMailerAutoload.php';

class Mailer{
	private $credentials;
	private static $instance;

	public static function GetInstance(){
		if(Mailer::$instance == null){
			Mailer::$instance = new Mailer();
		}
		return Mailer::$instance;
	}

	private function __construct(){
		$this->credentials = $GLOBALS['app_vars']['email_credentials_arguments'];
	}

	private function buildMailer(){
		$phpmailer = new PHPMailer(true);
		$phpmailer->IsSMTP();
		$phpmailer->From = $this->credentials['from'] ;
		$phpmailer->FromName = $this->credentials['from_name'];
		$phpmailer->AddReplyTo($this->credentials['from']);
		$phpmailer->Host = $this->credentials['host'];
		$phpmailer->Port = $this->credentials['port'];
		$phpmailer->Username = $this->credentials['username'];
		$phpmailer->Password = $this->credentials['password'];
		$phpmailer->IsHTML(true);
		return $phpmailer;
	}


	public function sendMessage($message, $subject, $emails){
		$mail = $this->buildMailer();

		foreach ($emails as $address) {
			$mail->addAddress($address);
		}

		$mail->Subject = $subject;
		$mail->Body = $message;
		//$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

		if(!$mail->send()){
			echo "<br>NOOOOOOOOO!!!!<br>" . $mail->ErrorInfo . "<br>";
		}

	}
}

?>