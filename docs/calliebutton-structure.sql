-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 20, 2014 at 12:05 AM
-- Server version: 5.5.36
-- PHP Version: 5.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `calliebutton`
--
CREATE DATABASE IF NOT EXISTS `calliebutton` DEFAULT CHARACTER SET utf8 COLLATE utf8_czech_ci;
USE `calliebutton`;

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
CREATE TABLE IF NOT EXISTS `course` (
  `uuid` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `start_date` date NOT NULL,
  `users_can_enroll` tinyint(1) NOT NULL DEFAULT '1',
  `line` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`line`),
  UNIQUE KEY `uuid_UNIQUE` (`uuid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `enrollment`
--

DROP TABLE IF EXISTS `enrollment`;
CREATE TABLE IF NOT EXISTS `enrollment` (
  `uuid` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `first` varchar(45) COLLATE utf8_czech_ci NOT NULL,
  `last` varchar(45) COLLATE utf8_czech_ci NOT NULL,
  `department` varchar(45) COLLATE utf8_czech_ci NOT NULL,
  `course_name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `course_id` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `reason_id` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `line` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`line`),
  UNIQUE KEY `uuid_UNIQUE` (`uuid`),
  KEY `fk_enrollment_course_idx` (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

DROP TABLE IF EXISTS `permission`;
CREATE TABLE IF NOT EXISTS `permission` (
  `role` varchar(25) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `uuid` varchar(45) COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `first` varchar(45) COLLATE utf8_czech_ci NOT NULL,
  `last` varchar(45) COLLATE utf8_czech_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `line` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`line`),
  UNIQUE KEY `uuid_UNIQUE` (`uuid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=3 ;

--
-- Triggers `user`
--
DROP TRIGGER IF EXISTS `user_BINS`;
DELIMITER //
CREATE TRIGGER `user_BINS` BEFORE INSERT ON `user`
 FOR EACH ROW BEGIN
	DECLARE newid char(32);
	SET newid = uuid();
	while((SELECT COUNT(uuid) FROM user WHERE uuid=newid) > 0) DO
		SET newid = uuid();
	END WHILE;

	SET new.uuid = newid;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `user_permission`
--

DROP TABLE IF EXISTS `user_permission`;
CREATE TABLE IF NOT EXISTS `user_permission` (
  `user_id` int(11) NOT NULL,
  `role` varchar(45) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`user_id`,`role`),
  KEY `fk_userpermission_permission_idx` (`role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `enrollment`
--
ALTER TABLE `enrollment`
  ADD CONSTRAINT `fk_enrollment_course` FOREIGN KEY (`course_id`) REFERENCES `course` (`uuid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_permission`
--
ALTER TABLE `user_permission`
  ADD CONSTRAINT `fk_userpermission_permission` FOREIGN KEY (`role`) REFERENCES `permission` (`role`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
