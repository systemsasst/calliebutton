/*
This file contains the javascript to generate bootstrap alerts on the fly
*/

function generateAlert(messageHTML, type){
	$html = $("<div class='alert alert-" + type + "'><button type='button' class='close' data-dismiss='alert'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>" + messageHTML + "</div>");
	return $html;
}