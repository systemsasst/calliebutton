

//This function will update the global variables that the tables are based on
function updateData(){
	var comAjax = updateCourses();
	var userAjax = updateUsers();
	comAjax.done();

	return $.when(comAjax, userAjax);
}

//This function will update the global course listing
function updateCourses(){
	return $.post((webroot + '/data/courses/getCourses.php'),
		function(response){
				response = JSON.parse(response);
				if(response.error){
					var alert = generateAlert(response.data, 'danger');
					$('#courses-content').prepend(alert).hide().slideDown();
				}
				else{
					allCourses = response.data;
					allEnrollments = [];
					for(var c = 0; c < allCourses.length; c++){
						var course = allCourses[c];
						for(var e = 0; e < course.enrolled.length; e ++){
							allEnrollments.push(course.enrolled[e]);
						}
					}
				}
			});
}

function updateUsers(){
	return $.post((webroot + '/data/users/getUsers.php'),
		function(response){
				response = JSON.parse(response);
				if(response.error){
					var alert = generateAlert(response.data, 'danger');
					$('#user-content').prepend(alert).hide().slideDown();
				}
				else{
					allUsers = response.data;
				}
			});
}