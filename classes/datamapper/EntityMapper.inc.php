<?php
/**
* This file contains the class for EntityMapper
*/

/**
*
*/
abstract class EntityMapper{
  private $databaseFactory;
  protected $db;

  public function __construct(){
    $this->databaseFactory = DatabaseFactory::GetInstance();
    $this->db = $this->databaseFactory->getConnection();
  }

  public function __destruct(){
    //$this->$db->close();
  }

  public abstract function save(Model $model);
  public abstract function find($id);
  public abstract function all();
  public abstract function delete($id);
  public static function findModel($id){

  }
}


?>
