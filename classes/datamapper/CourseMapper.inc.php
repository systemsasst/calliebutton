<?php
	require_once "EntityMapper.inc.php";

	class CourseMapper extends EntityMapper{

		public function __construct(){
			parent::__construct();
		}


		public function save(Model $course){
			if(get_class($course) != "Course"){
				throw new RequiredModelException("Sorry, something went wrong. If the problem persists, please contact the administrator.","Expected a Course got a " . get_class($course));
			}
			
			$dbid = $course->getDBID();
			$startDate = $course->getStartDate()->format("Y-m-d");
			$enrollable = $course->getEnrollable();

			//Checking to see if the course is in the database
			$inserting = true;
			$stmt = $this->db->prepare("SELECT uuid FROM course WHERE uuid=?");
			$stmt->bind_param("s",$dbid);
			$stmt->execute();
			$stmt->store_result();
			$count = $stmt->num_rows();

			$inserting = ($count == 0);


			if($inserting){
				$stmt = $this->db->prepare("INSERT INTO course SET uuid=?, start_date=?, users_can_enroll=?");
				$stmt->bind_param("ssi", $dbid, $startDate, $enrollable);
				$stmt->execute();
			}
			else{
				$stmt = $this->db->prepare("UPDATE course SET start_date=?, users_can_enroll=? WHERE uuid=?");
				$stmt->bind_param("sis", $startDate, $enrollable, $dbid);
				$stmt->execute();
			}

			$stmt->free_result();
		}

		public function all(){
			$courses = array();
			$query = "SELECT uuid, start_date, users_can_enroll FROM course ORDER BY start_date ASC";
			$result = $this->db->query($query);
			
			while($row = $result->fetch_assoc()){
				$courses[] = new Course($row['uuid'], new DateTime($row['start_date']), (boolean) $row['users_can_enroll']);
			}

			return $courses;
		}

		public function allEnrollable(){
			$courses = array();
			$query = "SELECT uuid, start_date, users_can_enroll FROM course WHERE users_can_enroll=1 ORDER BY start_date ASC";
			$result = $this->db->query($query);
			
			while($row = $result->fetch_assoc()){
				$courses[] = new Course($row['uuid'], new DateTime($row['start_date']), (boolean) $row['users_can_enroll']);
			}

			return $courses;
		}

		public function find($id){
			$stmt = $this->db->prepare("SELECT uuid, start_date, users_can_enroll FROM course WHERE uuid=?");
			//echo($this->db->error);
			$stmt->bind_param('s', $id);
			$stmt->execute();

			$startDateString;
			$enrollable;
			$stmt->bind_result($id, $startDateString, $enrollable);
			$stmt->fetch();

			$c = new Course($id, new DateTime($startDateString), (boolean) $enrollable);
			$stmt->free_result();

			return $c;
		}


		public function delete($id){
			$stmt = $this->db->prepare("DELETE FROM course WHERE uuid=?");
			$stmt->bind_param('s', $id);
			$stmt->execute();
		}


	}
?>