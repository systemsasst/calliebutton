function getEnrollmentListingHTML(enrollments){
	var html = '';
	for(var i = 0; i < enrollments.length; i ++){
		var enrollment = enrollments[i];
		var dateString = '';
		if(enrollment.start_date != ''){
			var date = moment(enrollment.start_date);
			dateString = date.format('MM/DD/YYYY');
		}
		

		html += "<tr class='enrollment-row' data-dbid='" + enrollment.dbid + "'>";

		html += '<td>' + enrollment.name + '</td>';

		html += "<td>" + enrollment.department + "</td>";

		html += "<td>" + enrollment.email + "</td>";

		html += "<td>" + dateString + "</td>";

		html += "<td>" + enrollment.reason + "</td>";

		html += "<td><button class='delete-enrollment btn btn-sm btn-danger'><span class='fa fa-trash'></span></button></td>";

		html += '</tr>';
	}

	return html;
}

$('#course-modal').on('click','.delete-enrollment', function(e){
	e.preventDefault();
	var id = $(this).parents('.enrollment-row').attr('data-dbid');

	$.post(webroot + '/data/enrollments/deleteEnrollment.php',
		{id: id},
		function(response){
			response = JSON.parse(response);
			if(!response.error){
				var dbid = $('#course-modal').attr('data-dbid');
				updateCourseDetails(dbid);
			}
			else{
				var alert = generateAlert(response.data, 'danger');
				$('#course-modal .modal-body').prepend(alert).hide().slideDown();
			}
		});
});

$('#view-new-enrollments').on('click', function(e){
	e.preventDefault();

	if(updateNewEnrollmentModal()){
		$('#new-enrollment-modal').modal();
	}
});


$('.next-new-enrollment').on('click', function(e){
	e.preventDefault();
	var curEnrollmentID = $('#new-enrollment-modal').attr('data-cur-enrollment-dbid');

	$.post(webroot + '/data/enrollments/setEnrollmentStatus.php',
		{id: curEnrollmentID, is_new: false},
		function(response){
			response = JSON.parse(response);

			if(!response.error){
				updateCourses().done(function(){
					updateNewEnrollmentModal();
				});
				
			}
		});
});



function updateNewEnrollmentModal(){
	var count = countNewEnrollments();
	$('.number-new-enrollments').text(count);

	if(count === 0){
		$('#new-enrollment-modal').modal('hide');
		return false;
	}

	var enrollment = getNextNewEnrollment();

	$('#new-enrollment-modal').attr('data-cur-enrollment-dbid', enrollment.dbid);
	$('#new-enrollment-modal .address').attr('href', 'mailto:' + enrollment.email + '?subject=' + encodeURIComponent('Eastern Online Course'));
	$('#new-enrollment-modal .address').text(enrollment.email);

	$.post(webroot + 'data/enrollments/getNewEnrollmentEmail.php',
			{id: enrollment.dbid},
			function(response){
				$('.message-body').html(response);
			});

	return true;
}

function countNewEnrollments(){
	var count = 0;
	for(var e = 0; e < allEnrollments.length; e++){
		if(allEnrollments[e].is_new){
			count ++;
		}
	}
	return count;
}

function getNextNewEnrollment(){
	for(var e = 0; e < allEnrollments.length; e++){
		if(allEnrollments[e].is_new){
			return allEnrollments[e];
		}
	}

	return false;
}
