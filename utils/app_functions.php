<?php
/**
* Miscilaneous functions for the application
*/



/**
* searches a directory and subdirectories for files to require_once
*/
function requireFilesInDirectory($dir, $subDirs = true, $ext = null){
  $files = array();
  foreach(scandir($dir) as $filename){
    $path = $dir . '/' . $filename;

    if(is_file($path)){
      if($ext == null || pathinfo($filename)['extension'] == $ext){
        //echo $path . ' - ' . pathinfo($filename)['extension'] . ' - ' . $ext . '<br><br><br>';
        $files[] = $path;
      }
    }
    else if($subDirs && substr($path, -1) != "." && substr($path, -2) != ".."){
      requireFilesInDirectory($path, $subDirs, $ext);
    }
  }

  foreach($files as $file){
   include_once $file;
  }
}

?>
