<?php

class EmailFactory{



	public function getNewEnrollmentEmail(Enrollment $enrollment){
		$course = $enrollment->getCourse();
		$name = $enrollment->getName();
		$course_date = $course->getStartDate()->format('m/d/Y');



		return "
				Dear $name,<br/>
				Thank you for registering for the Online Faculty Orientation Course on $course_date.  You will receive a welcome email from the instructor one week before the course begins with instructions on how to access the Canvas course. We look forward to working with you to create a high quality online course. If you have any questions, please do not hesitate to contact me. <br><br>
				Sincerely, <br>
				Tim Snyder (Director of Eastern Online) 
				";
	}

	public function getNewEnrollmentNotification(Enrollment $enrollment, $nameOfAdmin){
		$course = $enrollment->getCourse();
		$name = $enrollment->getName();
		$course_date = $course->getStartDate()->format('m/d/Y');

		return "
			Hi $nameOfAdmin!<br/>
			This is a notification that $name has enrolled for the Online Development class on $course_date.<br>
			<br>
			Thanks!<br/><br>
			<b>CallieButton System</b>
		";
	}
}

?>