<?php
	require_once "Model.inc.php";
	class Course extends Model{
		private $startDate;
		private $enrollable;

		public function __construct($id = null, DateTime $startDate = null, $enrollable = false){
			parent::__construct(new CourseMapper(), "course", $id);

			$this->startDate = $startDate;
			$this->enrollable = $enrollable;

			/*$valid = $this->validate();
			if($valid !== true){
				$userMessage = "There was an issue creating the course, please check your information and submit again:\n";
				$internalMessage = "Bad course!\n";

				foreach ($valid as $error) {
					if($error['attribute'] != "dbid"){
						$userMessage .= $error['error'] . "\n";
					}
					$internalMessage = $error['attribute'] . " - " . $error['error'] . "\n";
				}

				throw new AppException($internalMessage, $userMessage);
			}*/
		}

		public function validate(){
			$errors = parent::validate();

			if(!is_object($this->startDate) || get_class($this->startDate) != "DateTime"){
        		$errors[] = array("attribute" => "startDate", "error" => "Please enter a valid date.");
			}
			if(count($errors) == 0){
				return true;
			}

			return $errors;
		}

		public function getStartDate(){
			return $this->startDate;
		}

		public function setStartDate(DateTime $d){
			$this->startDate = $d;
		}

		public function getEnrollable(){
			return $this->enrollable;
		}

		public function setEnrollable($e){
			$this->enrollable = (((boolean) $e) === TRUE);
		}


		public function getEnrollments(){
			return (new EnrollmentMapper())->findByCourse($this->id);
		}




		public function getModelAsArray(){
			$enrolled = $this->getEnrollments();
			$enrolledArray = array();

			foreach ($enrolled as $e) {
				$enrolledArray[] = $e->getModelAsArray();
			}

			$info = array(
							'dbid' => $this->id,
							'startDate' => $this->startDate->format('Y-m-d'),
							'enrollable' => $this->enrollable,
							'enrolled' => $enrolledArray
						);
			return $info;
		}
	}
?>