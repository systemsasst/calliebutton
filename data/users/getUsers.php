<?php
//Getting the config
require_once '../../application/configuration.php';

$error = false;
$data = "";

startSession();
if(!validateCurrentUser()){
	$error = true;
	$data = "You must be logged in to do that!";
}


if(!$error){
	$data = array();
	$email = ''; 
	$name = '';
	$id = '';
	$db = DatabaseFactory::GetInstance()->getConnection();
	$user_id = $_SESSION[$GLOBALS['app_vars']['session_keys']['user_id']];
	$query_string = "SELECT uuid, CONCAT(first, ' ', last) AS name, email FROM user WHERE uuid != ?";

	$stmt = $db->prepare($query_string);
	$stmt->bind_param('s',$user_id);
	$stmt->execute();
	$stmt->bind_result($id, $name, $email);
	while($stmt->fetch()){
		$data[] = array("id" => $id,"name" => $name, "email" => $email);
	}
	$stmt->free_result();
}


echo json_encode(array('error' => $error, 'data' => $data));
?>