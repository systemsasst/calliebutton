<?php
/**
* This is HTML that is meant to be included with a page that displays this modal dialog from bootstrap
* This modal is for confirming that the user wants to delete a course
*/
?>


<div id='delete-course-modal' data-dbid='' class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content panel-danger">
      <div class="modal-header panel-heading">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Delete course on <span class='course-date'></span>?</h4>
      </div>
      <div class="modal-body">

      	<p>Are you sure you want to delete the course on <span class='course-date'></span>? All enrollments for that course will be deleted as well.</p>	

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        <button type="button" class="btn btn-danger confirm-delete-course" data-dismiss="modal">Yes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->