<?php
  /**
  * This file contains the class for the database connection
  */

  /**
  * This class is a singleton factory. Its purpose is to create database connections
  */
  class DatabaseFactory{
    /**
    * Instance of the database object
    */
    private static $factoryInstance = null;
    private $db = null;

    /**
    * Private constructor for the singleton
    */
    private function __construct(){
      $conn_info = $GLOBALS['app_vars']['database_connection_arguments'];
      $this->db = new mysqli($conn_info['address'], $conn_info['user'], $conn_info['pass'], $conn_info['database']);
    }

    public function __destruct(){
      $this->db->close();
    }

    /**
    * This method will return the database singleton
    */
    public static function GetInstance(){
      if(DatabaseFactory::$factoryInstance == null){
        DatabaseFactory::$factoryInstance = new DatabaseFactory();
      }

      return DatabaseFactory::$factoryInstance;
    }


    public function getConnection(){
      if($this->db != null && !$this->db->connect_errno){
        return $this->db;
      }
      else if($this->db->connect_errno){
        throw new AppException("Bad DB Connection!", "Sorry, there is an issue with the server, please try again later.");
      }
    }
  }
?>
