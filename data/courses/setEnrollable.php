<?php
	require_once '../../application/configuration.php';
	$error = false;
	$data = array();

	startSession();

	if(!validateCurrentUser()){
		$error = true;
		$data = "You must be logged in to do that.";
	}

	$id = isset($_POST['id']) ? $_POST['id'] : null;
	$enrollable = (isset($_POST['enrollable']) && $_POST['enrollable'] == 'true') ? true : false;

	if(!$error && $id == null){
		$error = true;
		$data = "Please provide a course to set enrollable on!";
	}

	if(!$error){
		try{
			$cm = new CourseMapper();
			$c = $cm->find($id);
			$c->setEnrollable($enrollable);
			$c->save();
			$data = $c->getDBID() . ' - ' . gettype($enrollable);
		}
		catch(AppException $e){
			$error = true;
			$data = $GLOBALS['app_vars']['debug'] ? $e->getMessage() : $e->getUserMessage();
		}
		catch(Exception $e){
			$error = true;
			$data = $GLOBALS['app_vars']['debug'] ? "Exception in getCourses! <br>\n" . $e->getMessage() . "<br/>\n" . $e->getTraceAsString(): $GLOBALS['app_vars']['strings']['general_error'];
		}
	}


	echo json_encode(array('error' => $error, 'data' => $data));
?>