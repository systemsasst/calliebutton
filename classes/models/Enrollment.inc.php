<?php
	require_once 'Model.inc.php';

	class Enrollment extends Model{

		private $course_id;
		private $email;
		private $name;
		private $department;
		private $course_name;
		private $start_date;
		private $end_date;
		private $reason;
		private $is_new;
		private $signed_up_on;

		public function __construct($id = null, $course_id = null, $email = null, $name = null, $department = null, $course_name = null, DateTime $start_date = null, DateTime $end_date = null, $reason = null, $is_new = true, $signed_up_on = null){
			parent::__construct(new EnrollmentMapper(), "enrollment", $id);

			$this->course_id = $course_id;
			$this->email = $email;
			$this->name = $name;
			$this->department = $department;
			$this->course_name = $course_name;
			$this->start_date = $start_date;
			$this->end_date = $end_date;
			$this->reason = $reason;
			$this->is_new = $is_new;

			//If the id is null, then the enrollment is new
			if($id == null && $signed_up_on == null){
				$this->signed_up_on = new DateTime();
			}
			else{
				$this->signed_up_on = $signed_up_on;
			}
			


			/*$valid = $this->validate();
			if($valid !== true){
				$userMessage = "There was an issue enrolling you in that course, please check your information and submit again:\n";
				$internalMessage = "Bad enrollment!\n";

				foreach ($valid as $error) {
					if($error['attribute'] != "dbid"){
						$userMessage .= $error['error'] . "\n";
					}
					$internalMessage = $error['attribute'] . " - " . $error['error'] . "\n";
				}

				throw new AppException($internalMessage, $userMessage);
			}*/
		}

		public function validate(){
			$errors = parent::validate();

			//Checking the course
			try{
				$course = (new CourseMapper())->find($this->course_id);
				if($course == null){
					throw new Exception("No course");
				}
				if($course->getDBID() != $this->course_id){
					throw new Exception("No course");
				}
			}
			catch(Exception $e){
				$errors[] = array("attribute" => "Class", "error" => "That class does not exist!");
			}
			//Checking the email
			if(!preg_match($GLOBALS['app_vars']['regexes']['email'], $this->email)){
        		$errors[] = array("attribute" => "Email", "error" => "Please enter a valid email.");
			}
			//Checking the name
			if(!preg_match($GLOBALS['app_vars']['regexes']['simple_string'], $this->name) || strlen($this->name) > 100 || strlen($this->name) < 1){
        		$errors[] = array("attribute" => "Name", "error" => "Please enter a valid name, less than 45 characters.");
			}
			//Checking the department
			if(!preg_match($GLOBALS['app_vars']['regexes']['simple_string'], $this->department) || strlen($this->department) > 45 || strlen($this->department) < 1){
        		$errors[] = array("attribute" => "Department", "error" => "Please enter a valid department, less than 45 characters.");
			}
			//Checking the course name
			if(!preg_match($GLOBALS['app_vars']['regexes']['simple_string'], $this->course_name) || strlen($this->course_name) > 255 || strlen($this->course_name) < 1){
        		$errors[] = array("attribute" => "Course Name", "error" => "Please enter a course name, less than 255 characters.");
			}
			//Checking the start date
			if($this->start_date != null && (!is_object($this->start_date) || get_class($this->start_date) != "DateTime")){
        		$errors[] = array("attribute" => "Start Date", "error" => "Please enter a valid start date.");
			}
			//Checking the end date
			if($this->end_date != null && (!is_object($this->end_date) || get_class($this->end_date) != "DateTime")){
        		$errors[] = array("attribute" => "End Date", "error" => "Please enter a valid end date.");
			}
			//Checking the reason string
			if(!preg_match($GLOBALS['app_vars']['regexes']['simple_string'], $this->reason) || strlen($this->reason) > 100 || strlen($this->reason) < 1){
        		$errors[] = array("attribute" => "Reason", "error" => "Please select a valid reason.");
			}



			if(count($errors) == 0){
				return true;
			}

			return $errors;
		}




		public function getEmail(){
			return $this->email;
		}

		public function setEmail($email){
			$this->email = $email;
		}

		public function getName(){
			return $this->name;
		}

		public function setName($name){
			$this->name = $name;
		}

		public function getCourseID(){
			return $this->course_id;
		}

		public function getCourse(){
			return (new CourseMapper())->find($this->course_id);
		}

		public function setCourseID($course_id){
			$this->course_id = $course_id;
		}

		public function getDepartment(){
			return $this->department;
		}

		public function setDepartment($department){
			$this->department = $department;
		}

		public function getCourseName(){
			return $this->course_name;
		}

		public function setCourseName($course_name){
			$this->course_name = $course_name;
		}

		public function getStartDate(){
			return $this->start_date;
		}

		public function setStartDate($start_date){
			$this->start_date = $start_date;
		}

		public function getEndDate(){
			return $this->end_date;
		}

		public function setEndDate($end_date){
			$this->end_date = $end_date;
		}

		public function getReason(){
			return $this->reason;
		}

		public function setReason($reason){
			$this->reason = $reason;
		}

		public function getIsNew(){
			return $this->is_new;
		}

		public function setIsNew($is_new){
			$this->is_new = $is_new;
		}

		public function getSignedUpOn(){
			return $this->signed_up_on;
		}




		public function getModelAsArray(){
			$info = array(
				'dbid' => $this->id,
				'course_id' => $this->course_id,
				'email' => $this->email,
				'name' => $this->name,
				'department' => $this->department,
				'course_name' => $this->course_name,
				'start_date' => ($this->start_date != null) ? $this->start_date->format('Y-m-d') : '',
				//'end_date' => ($this->end_date != null) ? $this->end_date->format('Y-m-d') : '',
				'reason' => $this->reason,
				'is_new' => $this->is_new,
				'signed_up_on' => ($this->signed_up_on != null) ? $this->signed_up_on->format('Y-m-d H:i:s') : ''
			);

			return $info;
		}
	}
?>