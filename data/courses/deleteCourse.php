<?php
/**
* This script will delete a course from the database
*/

//Getting the config
require_once '../../application/configuration.php';

$error = false;
$data = array();
$mapper = new CourseMapper();

//The database id of the course
$id = isset($_POST['id']) ? $_POST['id'] : null;


startSession();
if(!validateCurrentUser()){
	$error = true;
	$data = "You must be logged in to do that!";
}

if(!$error){
	try{
		$mapper->delete($id);
	}
	catch(Exception $e){
		$error = true;
		$data = "Sorry, there was an issue deleting that class";
	}
}

echo json_encode(array('error' => $error, 'data' => $data));
?>