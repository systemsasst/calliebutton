<?php
/**
* This script will return a form email for a new enrollment based on the id of the enrollment
*/


//Getting the config
require_once '../../application/configuration.php';

$error = false;
$data = "There was a problem!";
$enrollmentMapper = new EnrollmentMapper();

//The database id of the enrollment
$id = isset($_POST['id']) ? $_POST['id'] : null;
if($id == null){
	//Trying get if post was empty
	$id = isset($_GET['id']) ? $_GET['id'] : null;
}

$enrollment;

startSession();
if(!validateCurrentUser()){
	$error = true;
	$data = "You must be logged in to do that!";
}

if(!$error){
	if($id != null){
		$enrollment = $enrollmentMapper->find($id);
		if($enrollment == null){
			$error = true;
			$data = "Please select a valid enrollment!";
		}
	}
	else{
		$error = true;
		$data = "You must select an enrollment to do that!";
	}
}

if(!$error){
	$emailFactory = new EmailFactory();
	echo $emailFactory->getNewEnrollmentEmail($enrollment);
}
else{
	echo $data;
}


?>

