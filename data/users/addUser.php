<?php
//Getting the config
require_once '../../application/configuration.php';

$error = false;
$data = "";
$tempPass;

startSession();
if(!validateCurrentUser()){
	$error = true;
	$data = "You must be logged in to do that!";
}


$first = isset($_POST['first-name']) ? $_POST['first-name'] : null;
$last = isset($_POST['last-name']) ? $_POST['last-name'] : null;
$email = isset($_POST['email']) ? $_POST['email'] : null;

if($first == NULL || $last == NULL || $email == NULL){
	$error = true;
	$data = "Please fill out all fields!";
}

if(!$error){
	if(!preg_match($GLOBALS['app_vars']['regexes']['email'], $email)){
		$error = true;
		$message = "Please enter a valid email!";
	}
}

if(!$error){
	$ph = new PasswordHash($GLOBALS['app_vars']['phpass']['log2'], $GLOBALS['app_vars']['phpass']['mobile']);

	$tempPass = generatePassword();
	$hashed = $ph->HashPassword($tempPass);

	try{
		$db = DatabaseFactory::GetInstance()->getConnection();

		$query_stmt = "INSERT INTO user SET email=?, first=?, last=?, password=?";
		$stmt = $db->prepare($query_stmt);
		$stmt->bind_param("ssss", $email, $first, $last, $hashed);
		if(!$stmt->execute()){
			throw new AppException("COULD NOT ADD USER!<br>\r\n" . $stmt->error, "Sorry, there was an issue. Please try again.");
		}

		$data = "User created! <br>Email: $email<br>Temporary Password: $tempPass";
	}
	catch(AppException $e){
		$error = true;
		if($GLOBALS['app_vars']['debug']){
			$data = $e->getMessage();
		}
		else{
			$data = $e->getUserMessage();
		}
	}
	catch(Exception $e){
		$error = true;
		$data = "Sorry, there was an issue.";
	}
}



echo json_encode(array('error' => $error, 'data' => $data));
?>