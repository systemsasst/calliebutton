<?php
/**
* This script will register a user for a course
* It does not require the user to be logged in
*/

require_once './application/configuration.php';

$error = false;
$data = "";


$course_id = isset($_POST['id']) ? $_POST['id'] : null;
$name = isset($_POST['name']) ? $_POST['name'] : null;
$department = isset($_POST['department']) ? $_POST['department'] : null;
$email = isset($_POST['email']) ? $_POST['email'] : null;
$reason = isset($_POST['reason']) ? $_POST['reason'] : null;
$course_name = isset($_POST['course']) ? $_POST['course'] : null;
$start_date = isset($_POST['start_date']) ? $_POST['start_date'] : null;
$end_date = isset($_POST['completion_date']) ? $_POST['completion_date'] : null;


try{
	if($start_date != null){
		$start_date = new DateTime($start_date);
	}
	if($end_date != null){
		$end_date = new DateTime($end_date);
	}

	$enrollment = new Enrollment(/*ID of the enrollment must be null*/ null, $course_id, $email, $name, $department, $course_name, $start_date, $end_date, $reason);
	$enrollment->save();

	$emailer = Mailer::GetInstance();
	$emailBuilder = new EmailFactory();

	$message = $emailBuilder->getNewEnrollmentEmail($enrollment);
	$emailer->sendMessage($message, "Enrollment for Online Development Course", [$enrollment->getEmail()]);

	//Sending the admins a notification
	$users = getAllUsers();
	foreach ($users as $key => $user) {
		$message = $emailBuilder->getNewEnrollmentNotification($enrollment, $user['first']);
		$emailer->sendMessage($message, "New Enrollment for Online Development Course", [$user['email']]);
	}

}
catch(AppException $e){
	$error = true;
	$data = $GLOBALS['app_vars']['debug'] ? $e->getMessage() : $e->getUserMessage();
}
catch(Exception $e){
	$error = true;
	$data = $GLOBALS['app_vars']['general_error'];
}

echo json_encode(array('error' => $error, 'data' => $data));
?>