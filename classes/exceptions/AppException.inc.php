<?php
  /**
  * This file contains the class AppException
  */

  /**
  * Custom exception for this application
  * All other custom exceptions will inheret from this
  * Automatically logs the error
  */
  class AppException extends Exception{
    private $user_message;

    public function __construct($internal_message = null, $user_message = null, $code = 0, Exception $previous = null){
      parent::__construct($internal_message, $code, $previous);
      $this->user_message = $user_message;

      //If the log directory does not exist, attempt to create it
      /*f(!file_exists($GLOBALS['app_vars']['log_directory'] . '/errors/')){
        try{
          mkdir($GLOBALS['app_vars']['log_directory'] . '/errors/', 0775, true);
        }
        catch(Exception $e){
          throw new Exception("Could not create log file path!");
        }
      }

      $log_entry = "Exception thrown - " . get_class($this) . " - " . $date = date('m/d/Y h:i:s a', time()) . "\r\n" . $this->getMessage() . "\r\n" . $this->getTraceAsString() . "\r\n";
      if(isset($SESSION['user_name'])){
        $log_entry = $log_entry . "\r\nUser: " . $SESSION['user_name'];
      }

      $file_name = $GLOBALS['app_vars']['log_directory'] . '/errors/' . "Error - " . date('m-d-Y his') . ".txt";

      try{
        $file = fopen($file_name, "w");
        fwrite($file, $log_entry);
        fclose($file);
      }
      catch(Exception $e){
        throw new Exception("Could not write log file!");
      }*/

    }

    public function getUserMessage(){
      return $this->user_message;
    }
  }
?>
