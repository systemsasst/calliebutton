<?php
//Getting the config
require_once '../../application/configuration.php';

$error = false;
$data = "";

//The id of the user
$id = isset($_POST['id']) ? $_POST['id'] : null;


startSession();
if(!validateCurrentUser()){
	$error = true;
	$data = "You must be logged in to do that!";
}

if(!preg_match($GLOBALS['app_vars']['regexes']['dbid'], $id)){
	$error = true;
	if($GLOBALS['app_vars']['debug']){
		$data = "Bad uuid! - " . $id;
	}
	else{
		$data = $GLOBALS['app_vars']['general_error'];
	}
}



if(!$error){
	$query = "DELETE FROM user WHERE uuid=?";
	$db = DatabaseFactory::GetInstance()->GetConnection();
	$stmt = $db->prepare($query);
	$stmt->bind_param('s',$id);
	if(!$stmt->execute()){
		$error = false;
		$data = "There was an issue deleteing the user.";
		if($GLOBALS['app_vars']['debug']){
			$data .= "<br/>\r\n>" . $stmt->error;
		}
	}
	else{
		$data = "User deleted!";
	}
}


echo json_encode(array("error" => $error, "data" => $data));
?>