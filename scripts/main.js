$(document).ready(function() {

    /* Change Menu li To Active when Clicked */
    $(".icon-menu li").on("click", function() {
       if(!$(this).hasClass("active"))
       {
           $(".active").removeClass("active");
           $(this).addClass("active");  
       }
    });
    
    $("#mobile-nav").on("click", function() {
        $(this).parent().parent().toggleClass("active");
    });


});