<?php
  /**
  * This file contains the RequiredModelException class
  */
  require_once 'AppException.inc.php';

  /**
  * The RequiredModelException is thrown when the correct model type (or no model) is found but required
  */
  class RequiredModelException extends AppException{
    public function __construct($expecetd_type, $got_type, $code = 0, Exception $previous = null){

      $user_message = "Sorry, there was an issue. Please try again. If the issue persists, please contact the system administrator.";
      $internal_message = "Wrong model type! Expected $expected_type - got $got_type";
      parent::__construct($internal_message, $user_message, $code, $previous);
    }
  }
?>
