<?php
	require_once '../application/configuration.php';
	resetSession();

	$in_email = null;
	$in_pass = null;
	$db_id = null;
	$db_pass = null;
	//This file will process the login for the user
	//Will return a JSON response:
	/*
		error: boolean
		message: string
	*/
	$error = false;
	$message = 'There was an issue processing yor log in. Please try again.';//Default error

	

	$db = DatabaseFactory::GetInstance()->getConnection();
	$ph = new PasswordHash($GLOBALS['app_vars']['phpass']['log2'], $GLOBALS['app_vars']['phpass']['mobile']);



	//Checking for email and password POSTs
	if(!isset($_POST['email']) || !isset($_POST['pass'])){
		$error = true;
	}
	else{
		$in_email = $_POST['email'];
		$in_pass = $_POST['pass'];
	}

	if(!$error){
		//checking to make sure only an email is entered
		$error = !preg_match($GLOBALS['app_vars']['regexes']['email'], $in_email);
		$message = "Please enter an email address.";
	}

	if(!$error){
		//Querying if the email is in the database and pulling the hashed password
		$stmt = $db->prepare('SELECT uuid, password FROM user WHERE email=?');
		$stmt->bind_param('s',$in_email);
		$stmt->execute();
		$stmt->store_result();
		if($stmt->num_rows != 1){
			$error = true;
			$message = 'Sorry, there is no user with those credentials.';
		}
		else{
			$stmt->bind_result($db_id, $db_pass);
			$stmt->fetch();
		}
	}

	//Now checking the passwords
	if(!$error){
		$res = $ph->CheckPassword($in_pass, $db_pass);

		//If passwords don't match
		if(!$res){
			$error = true;
			$message = 'Sorry, there is no user with those credentials.';
		}
		else{
			//Everything is good
			$_SESSION[$GLOBALS['app_vars']['session_keys']['user_name']] = $in_email;
			$_SESSION[$GLOBALS['app_vars']['session_keys']['user_id']] = $db_id;
		}
	}
	




	unset($ph);
	$ph = null;


	//Finally printing the json. 
	//If there were no issues, the user exists and the login page will redirect to the index page
	echo json_encode(array('problem' => $error, 'message' => $message));
?>