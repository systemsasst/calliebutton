$('#change-password-button').on('click', function(e){
	e.preventDefault();
	$('.password-alert').slideUp().alert('close');
	var data = $('#change-password-form').serialize();

	$.post(webroot + '/data/users/changePassword.php',
		data,
		function(response){
			response = JSON.parse(response);

			if(!response.error){
				$('#change-password-form input').val('');
				var alert = generateAlert(response.data, 'success').addClass('password-alert');
				alert.hide();
				$('#password-content').prepend(alert);
				alert.slideDown();
			}
			else{
				var alert = generateAlert(response.data, 'danger').addClass('password-alert');
				alert.hide();
				$('#password-content').prepend(alert);
				alert.slideDown();
			}
		});
});

$('#add-user').on('click', function(e){
	e.preventDefault();

	$('#add-user-modal').modal();
});

$('#submit-new-user').on('click', function(e){
	e.preventDefault();

	var data = $('#new-user-form').serialize();

	$.post(webroot + '/data/users/addUser.php',
		data,
		function(response){
			response = JSON.parse(response);
			fillData($('#users tbody'), 'users');

			if(!response.error){
				$('#new-user-form input').val('');
				$('#add-user-modal').modal('hide');
				$('#add-user-success-modal .modal-body').html(response.data);
				$('#add-user-success-modal').modal();
			}
			else{
				var alert = generateAlert(response.data, 'danger');
				alert.hide();
				$('#add-user-modal .modal-body').prepend(alert);
				alert.slideDown();
			}
		});
});

$('body').on('click','.delete-user',function(e){
	e.preventDefault();

	var dbid = $(this).parents('.user-row').attr('data-dbid');
	//alert(dbid);

	$.post(webroot + '/data/users/deleteUser.php',
		{id: dbid},
		function(response){
			response = JSON.parse(response);
			fillData($('#users tbody'), 'users');

			if(!response.error){
				var alert = generateAlert(response.data, 'success');
				$('#user-content').prepend(alert);
				alert.slideDown();
			}
			else{
				var alert = generateAlert(response.data, 'danger');
				$('#user-content').prepend(alert);
				alert.slideDown();
			}
		});
});

$('body').on('click','.reset-user', function(e){
	e.preventDefault();

	var dbid = $(this).parents('.user-row').attr('data-dbid');

	$.post(webroot + '/data/users/resetPassword.php',
			{id: dbid},
			function(response){

				response = JSON.parse(response);
				if(!response.error){
					var alert = generateAlert(response.data, 'success');
					$('#user-content').prepend(alert);
					alert.slideDown();
				}
				else{
					var alert = generateAlert(response.data, 'danger');
					$('#user-content').prepend(alert);
					alert.slideDown();
				}
			})
});