<?php
//Getting the config
require_once '../../application/configuration.php';

$error = false;
$data = "";

//The start date of the course
$curPass = isset($_POST['cur-pass']) ? $_POST['cur-pass'] : null;
$newPass = isset($_POST['new-pass']) ? $_POST['new-pass'] : null;
$confPass = isset($_POST['confirm-pass']) ? $_POST['confirm-pass'] : null;


startSession();
if(!validateCurrentUser()){
	$error = true;
	$data = "You must be logged in to do that!";
}

if($curPass == NULL || $newPass == NULL || $confPass == NULL){
	$error = true;
	$data = "Please fill out all fields!";
}

try{
	$email = $_SESSION[$GLOBALS['app_vars']['session_keys']['user_name']];
	$id = $_SESSION[$GLOBALS['app_vars']['session_keys']['user_id']];
	//Checking the password confirmation
	if($newPass != $confPass){
		throw new Exception("Please make sure that both passwords are the same!");
	}

	//Checking the strength
	if(!preg_match($GLOBALS['app_vars']['regexes']['password'], $newPass) || strlen($newPass) < 8 || strlen($newPass) > 254){
		throw new Exception("The password must contain at least 3 of these: Upper Case Character, Lower Case Character, Number, and Symbol.<br>The password must be greater than 8 characters and less than 254 charracters.");
	}

	//Checking the current user's password
	$db = DatabaseFactory::GetInstance()->getConnection();
	$hashedFromDB = "";

	$query_string = "SELECT password FROM user WHERE email=? AND uuid=?";
	$stmt = $db->prepare($query_string);
	$stmt->bind_param('ss', $email, $id);
	$stmt->execute();

	$stmt->bind_result($hashedFromDB);
	try{
		$stmt->fetch();
	}
	catch(Exception $e){
		throw new AppException("BAD SESSION VARS IN CHANGE PASS! - $email - $id <br>\r\n " . $e->getMessage(),"Sorry, there was an issue processing your request. Please log out, log back in, and try again.", 0, $e);
	}

	$ph = new PasswordHash($GLOBALS['app_vars']['phpass']['log2'], $GLOBALS['app_vars']['phpass']['mobile']);
	if(!$ph->CheckPassword($curPass, $hashedFromDB)){
		throw new Exception("The current password you entered is incorrect!");
	}

	$stmt->free_result();

	//If we are here, then all data is valid and we can procede

	try{
		$hashed = $ph->HashPassword($newPass);
		$query_string = "UPDATE user SET password=? WHERE email=? AND uuid=?";
		$stmt = $db->prepare($query_string);
		$stmt->bind_param("sss", $hashed, $email, $id);
		$stmt->execute();

		$data = "Password updated!";
	}
	catch(Exception $e){
		throw new AppException("COULDN'T UPDATE PASSWORD! <br>\r\n " . $e->getMessage(),"Sorry, there was an issue processing your request. Please log out, log back in, and try again.", 0, $e);
	}
}
catch(AppException $e){
	$error = true;
	if($GLOBALS['app_vars']['debug']){
		$data = $e->getMessage();
	}
	else{
		$data = $e->getUserMessage();
	}
}
catch(Exception $e){
	$error = true;
	$data = $e->getMessage();
}


echo json_encode(array('error' => $error, 'data' => $data));

?>