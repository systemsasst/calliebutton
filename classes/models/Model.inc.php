<?php
  /**
  * This file contains the model class
  */

  /**
  * This is the primary class that any model will inheret from
  *
  */
  abstract class Model{
    private $mapper;
    protected $id;

    protected function __construct(EntityMapper $mapper, $prefix, $id = ''){
      if($id == null){
        $id = $this->getNewDBID($prefix);
      }

      $this->id = $id;

      $this->mapper = $mapper;
    }

    private function getNewDBID($prefix){
      return uniqid($prefix, true);
    }


    public function save(){
      $valid = $this->validate();
      if($valid === true){
        $this->mapper->save($this);
      }
      else{
        $userMessage = "Please check your information and submit again:\n";
        $internalMessage = "Invalid model! Cannot save!\n";

        foreach ($valid as $error) {
          if($error['attribute'] != "dbid"){
            $userMessage .= $error['error'] . "\n";
          }
          $internalMessage = $error['attribute'] . " - " . $error['error'] . "\n";
        }

        throw new AppException($internalMessage, $userMessage);
      }
    }

    protected function validate(){
      $errors = array();

      if(!preg_match($GLOBALS['app_vars']['regexes']['dbid'], $this->id)){
        $errors[] = array("attribute" => "dbid", "error" => "The database id is invalid!");
      }

      return $errors;
    }

    public function getDBID(){
      return $this->id;
    }

    public abstract function getModelAsArray();

  }
?>
