-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 20, 2014 at 12:06 AM
-- Server version: 5.5.36
-- PHP Version: 5.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `calliebutton`
--
CREATE DATABASE IF NOT EXISTS `calliebutton` DEFAULT CHARACTER SET utf8 COLLATE utf8_czech_ci;
USE `calliebutton`;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`uuid`, `start_date`, `users_can_enroll`, `line`) VALUES
('course54932344a1663', '2014-12-01', 1, 1),
('course54932372e4505', '2014-10-01', 0, 2),
('course5493239c03fdc5.55986514', '2015-01-01', 1, 4);

--
-- Dumping data for table `permission`
--

INSERT INTO `permission` (`role`) VALUES
('admin');

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`uuid`, `email`, `first`, `last`, `password`, `line`) VALUES
('e7ad70cc-862a-11e4-95cd-95b1b579', 'systemsasst@ewu.edu', 'Jacob', 'Schwartz', '$2a$08$BpVjQAVPrLLDY167p4191u6b54AufjjEYEwVpLipYzFnvUyMvDWXy', 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
