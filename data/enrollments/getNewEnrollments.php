<?php
require_once '../../application/configuration.php';
$error = false;
$data = array();
$mapper = new EnrollmentMapper();


startSession();
if(!validateCurrentUser()){
	$error = true;
	$data = "You must be logged in to do that!";
}

if(!$error){
	try{
		$enrollments = $mapper->findByStatus(true);
		foreach ($enrollments as $en) {
			$data[] = $en->getModelAsArray();
		}
		
	}
	catch(AppException $e){
		$error = true;
		$data = $GLOBALS['app_vars']['debug'] ? $e->getMessage() : $e->getUserMessage();
	}
	catch(Exception $e){
		$error = true;
		$data = $GLOBALS['app_vars']['debug'] ? "Exception in getCourses! <br>\n" . $e->getMessage() . "<br/>\n" . $e->getTraceAsString(): $GLOBALS['app_vars']['strings']['general_error'];
	}
	
}

echo json_encode(array("error" => $error, "data" => $data));
?>