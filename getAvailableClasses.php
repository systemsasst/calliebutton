<?php
require_once './application/configuration.php';
$error = false;
$data = array();
$mapper = new CourseMapper();


try{
	$courses = $mapper->allEnrollable();
	$courseData = array();
	foreach ($courses as $course) {
		$courseData[] = array('id' => $course->getDBID(), 'start_date' => $course->getStartDate()->format('Y-m-d'));
	}

	$data = $courseData;
}
catch(AppException $e){
	$error = true;
	$data = $GLOBALS['app_vars']['debug'] ? $error->getMessage() : $error->getUserMessage();
}
catch(Exception $e){
	$error = true;
	$data = $GLOBALS['app_vars']['general_error'];
}



echo json_encode(array('error' => $error, 'data' => $data));
?>