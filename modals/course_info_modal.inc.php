<?php
	/**
	* This is HTML that is meant to be included with a page that displays this modal dialog from bootstrap
	* This modal is for the extended look at a course and its enrollments
	*/
?>

<div id='course-modal' data-dbid='' class="modal fade">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Course on <span id='course-date'></span></h4>
      </div>
      <div class="modal-body">

      <table class='table table-condensed table-bordered table-striped'>
      	<thead>
      		<tr>
      		<th>Name</th>
      		<th>Department</th>
      		<th>Email</th>
      		<th>Start Date</th>
          
      		<th>Reason</th>
      		<th></th>
      	</tr>
      	</thead>
      	
      	<tbody id='enrollment-listing'>
      		
      	</tbody>
      </table>


      <button class='get-enrolled-csv btn btn-success' data-dbid=''>Download List</button>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

