<?php

/**
* This file contains the various regular expressions to be used in validation across the system
* They will be stored in an associative array, to make access easier
*/

$validation_regex = array();
//email
$validation_regex['email'] = '/^[a-z0-9!#$%&\'*+=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/';
//Pasword requires 3 of 4 (uppercase, lowercase, number, special)
$validation_regex['password']  = '/(?=^.{6,255}$)((?=.*\d)(?=.*[A-Z])(?=.*[a-z])|(?=.*\d)(?=.*[^A-Za-z0-9])(?=.*[a-z])|(?=.*[^A-Za-z0-9])(?=.*[A-Z])(?=.*[a-z])|(?=.*\d)(?=.*[A-Z])(?=.*[^A-Za-z0-9]))^.*/';
//Database id
$validation_regex['dbid'] = '/^[a-zA-Z\-\.0-9]+$/';
//Simple string matching alpha-numeric and some special characters
$validation_regex['simple_string'] = '/^[A-Za-z0-9\.\-\_\\/\(\)\s\\\'\"\,\!\?\:\;]+$/';
?>
