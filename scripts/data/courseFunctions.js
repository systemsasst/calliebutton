function enrollToggle(e, state){
	var dbid = $(this).parents('tr').attr('data-dbid');

	$.post(webroot + '/data/courses/setEnrollable.php',
		{id: dbid, enrollable: state},
		function(response){
			response = JSON.parse(response);
			if(response.error){
				//There was an issue setting the enrollable state
				$(this).toggleState();
				$(this).parents('.table-content').prepend(alert).hide().slideDown();
			}
		}
	);
}

$('body').on('click','.course-info-button', function(e){
	e.preventDefault();

	var dbid = $(this).parents('.course-row').attr('data-dbid');

	updateCourseDetails(dbid).done(function(){
		$('#course-modal').modal();
	});
});


function updateCourseDetails(dbid){
	return $.post((webroot + '/data/courses/getCourses.php'),
		{id: dbid},
		function(response){

			response = JSON.parse(response);
			if(!response.error){
				var course = response.data;

				var date = moment(course['startDate']);
				var dateString = date.format('MMMM Do, YYYY');

				$('#course-modal').attr('data-dbid',dbid);

				$('#course-modal #course-date').text(dateString);
				$('#course-modal .get-enrolled-csv').attr('data-dbid', course['dbid']);
				$('#enrollment-listing').html(getEnrollmentListingHTML(course['enrolled']));
			}
			else{
				var alert = generateAlert(response.data, 'danger');
				$('#course-modal .modal-body').prepend(alert).hide().slideDown();
			}
		});
}



$('body').on('click','.get-enrolled-csv', function(e){
	e.preventDefault();
	var dbid = $(this).attr('data-dbid');

	window.open(webroot + 'data/enrollments/getEnrollmentsAsCSV.php?id=' + dbid)
});

$('#add-course').on('click',function(e){
	e.preventDefault();

	$('#add-course-modal').modal();
});


$('body').on('click','.delete-course',function(e){
	e.preventDefault();
	var dbid = $(this).parents('.course-row').attr('data-dbid');
	var date = $(this).parents('.course-row').find('.course-date').text();

	$('#delete-course-modal').attr('data-dbid', dbid);
	$('#delete-course-modal .course-date').text(date);
	$('#delete-course-modal').modal();
});


function deleteCourse(dbid){
	return $.post(webroot + 'data/courses/deleteCourse.php',
		{id: dbid},
		function(response){
			response = JSON.parse(response);
			if(!response.error){
				fillData($('#courses tbody'), 'courses');
			}
			else{
				var alert = generateAlert(response.data, 'danger');
				$('#courses').prepend(alert).hide().slideDown();
			}
		});
}

$('.confirm-delete-course').on('click',function(e){
	e.preventDefault();

	var dbid = $('#delete-course-modal').attr('data-dbid');

	deleteCourse(dbid);

	$('#delete-course-modal').modal('hide');
});



$('.add-class').on('click', function(e){
	e.preventDefault();

	var startDate = $('#add-course-modal #course-date').val();

	$.post(webroot + '/data/courses/addCourse.php',
		{startDate: startDate},
		function(response){
			response = JSON.parse(response);
			if(!response.error){
				fillData($('#courses tbody'), 'courses');
				$('#add-course-modal').modal('hide');
			}
			else{
				var alert = $(generateAlert(response.data, 'danger'));

				$('#add-course-modal .modal-body').prepend(alert);

				alert.hide().slideDown();
			}
		});
});