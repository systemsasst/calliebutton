<?php
    //Getting the config
    require_once './application/configuration.php';
    startSession();
    if(!validateCurrentUser()){
        header("Location: login");
    }

    //Getting the number of new enrollments
    $enrollmentMapper = new EnrollmentMapper();
    $new_enrollment_count = count($enrollmentMapper->findByStatus(true));

    $en = new Enrollment(null, "course54af04ce4f8c55.03146230", "someone@test.com", "Some Person", "Engineering", "Intro. to Engineering", new DateTime("2015-04-01"), null, "Reason", true, null);
    //$enrollmentMapper->save($en);
?>

<!DOCTYPE html>
<html>

<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>CallieButton Admin</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <link rel="stylesheet" href="styles/main.css">
        <link rel="stylesheet" href="styles/panel.css">
        <link rel="stylesheet" href="bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
        <!-- endbuild -->
        <script src="bower_components/modernizr/modernizr.js"></script>
    </head>
    <body>


        <noscript>
            <div style="background: #444; color: #FFF; width: 100%; height: 50px; font-size: 20px; padding: 8px;">
                <center>Enable JavaScript in your browser to use this site.</center>
            </div>
        </noscript>
                

        <!-- MENU //-->
        <div id='navigation' class="menu-container col-md-1">
            <div class="mobile-menu">
            </div>

            <ul class="icon-menu">
                <li class="active" id="nav-courses" data-page='courses'> <a><i class="glyphicon glyphicon-book"></i> <span>Classes</span></a> </li>   
                <li id="nav-users" data-page='users'> <a><i class="fa fa-gear"></i> <span>Settings</span></a> </li>
                <li id="nav-logout"> <a href='login'><i class="fa fa-power-off"></i> <span>Logout</span> </a></li> 
            </ul>
        </div>
        <!-- Content Area //-->
        <div class="main-container col-md-11">
            <header>
                <h1>CallieButton <b>Admin</b></h1>
            </header>

            <aside class="content" id='main-content'>


            <!-- Classes -->
                <div id='courses' class='page'>
                    <!-- Courses -->
                    <div class='col-lg-12 col-sm-12'>
                        <div class="table-wrapper" id='courses-panel'>
                            <div class="table-title">
                                <h4>Classes</h4>
                                <div class='add-btn header-btn' id='add-course'>Add a Class</div>
                            </div>

                            <div class="table-content clearfix" id='courses-content'>

                               <div class='t-container col-md-12'>
                                   <table class='table table-condensed table-bordered table-striped listing' id='panel-courses-table'>
                                       <thead>
                                           <tr>
                                               <th>Date</th>
                                               <th>Number Enrolled</th>
                                               <th>Users Can Enroll</th> <!-- Toggle Available -->
                                               <th></th> <!-- More Info -->
                                               <th></th> <!-- Delete -->
                                           </tr>
                                           
                                       </thead>
                                       <tbody></tbody>
                                   </table>
                               </div>
                                
                            </div>
                        </div>
                    </div>
                    

                </div><!-- End #courses -->


                <div id='users' class='page'>
                    <div class='col-lg-8 col-sm-12'>
                        <div class="table-wrapper" id='users'>
                            <div class="table-title">
                                <h4>Users</h4>
                                <div class='add-btn header-btn' id='add-user'>Add a User</div>
                            </div>

                            <div class="table-content clearfix" id='user-content'>

                                <div class='t-container col-md-12'>
                                    <table class='table table-condensed table-bordered table-striped listing' id='panel-users-table'>
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                           
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                                
                            </div>
                        </div>
                    </div>


                    <div class='col-lg-4 col-sm-12'>
                        <div class="table-wrapper" id='password'>
                            <div class="table-title">
                                <h4>Change Password</h4>
                            </div>

                            <div class="table-content clearfix" id='password-content'>

                               <div class='t-container col-md-12'>
                                   <form id='change-password-form'>
                                       <div class='form-group'>
                                            <div class='input-group'>
                                                <div class='input-group-addon'>Current</div>
                                                <input class='form-control' type='password' id='cur-pass' name="cur-pass" />
                                            </div>
                                        </div>
                                       <div class='form-group'>
                                            <div class='input-group'>
                                                <div class='input-group-addon'>New</div>
                                                <input class='form-control' type='password' id='new-pass' name="new-pass" />
                                            </div>
                                        </div>
                                       <div class='form-group'>
                                            <div class='input-group'>
                                                <div class='input-group-addon'>Confirm</div>
                                                <input class='form-control' type='password' id='confirm-pass' name="confirm-pass" />
                                            </div>
                                       </div>
                                       <button id='change-password-button' class='btn btn-warning col-md-12'>Change your password</button>
                                   </form>
                               </div>
                                
                            </div>
                        </div>
                    </div>
                </div><!-- End #settings -->



            </aside><!-- End Content //-->
        </div> <!-- End .main-container //-->
                
        <?php require_once './include_modals.inc.php' ?>



        <!-- <script src="bower_components/jquery/jquery.js"></script> -->
        <script src='https://code.jquery.com/jquery-2.1.1.min.js'></script>

        <script src="scripts/pace.js"></script>
        <script src="scripts/main.js"></script>

        <script src="bower_components/bootstrap-sass/assets/javascripts/bootstrap/affix.js"></script>
        <script src="bower_components/bootstrap-sass/assets/javascripts/bootstrap/alert.js"></script>
        <script src="bower_components/bootstrap-sass/assets/javascripts/bootstrap/dropdown.js"></script>
        <script src="bower_components/bootstrap-sass/assets/javascripts/bootstrap/tooltip.js"></script>
        <script src="bower_components/bootstrap-sass/assets/javascripts/bootstrap/modal.js"></script>
        <script src="bower_components/bootstrap-sass/assets/javascripts/bootstrap/transition.js"></script>
        <script src="bower_components/bootstrap-sass/assets/javascripts/bootstrap/button.js"></script>
        <script src="bower_components/bootstrap-sass/assets/javascripts/bootstrap/popover.js"></script>
        <script src="bower_components/bootstrap-sass/assets/javascripts/bootstrap/collapse.js"></script>
        
        <script src="bower_components/bootstrap-switch/dist/js/bootstrap-switch.js"></script>


        <script src="scripts/data/getData.js"></script>
        <script src="scripts/data/tables.js"></script>
        <script src="scripts/data/courseFunctions.js"></script>
        <script src="scripts/data/enrollmentFunctions.js"></script>
        <script src="scripts/data/userFunctions.js"></script>
        <script src="scripts/alerts.js"></script>
        <script src="scripts/moment.js"></script>


        <script type="text/javascript">
            var webroot = '/<?php echo $GLOBALS['app_vars']['base_url'] ?>/';
            var allCourses;
            var allUsers;
            var allEnrollments;


            fillData($('#courses tbody'), 'courses');
            fillData($('#users tbody'), 'users');

            $('.page').hide();
            $('#courses').slideDown();
            



            //Changing pages
            $('#navigation li').on('click', function(){
                $('.page').slideUp();
                $('#' + $(this).attr('data-page')).slideDown();
                fillData($('#' + $(this).attr('data-page') + ' tbody'), $(this).attr('data-page'));
            });



            //Making the whole logout button work, not just the text
            $('#nav-logout').on('click', function(){
                    window.location = 'login';
                }
            );
        </script>
    </body>

</html>