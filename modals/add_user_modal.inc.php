<?php
/**
* This is HTML that is meant to be included with a page that displays this modal dialog from bootstrap
* This modal will contain the form to add a user.
*/
?>

<div id='add-user-modal' class="modal fade">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add a user</h4>
      </div>
      <div class="modal-body">

      <form id='new-user-form'>
      	<div class='form-group'>
      		<div class='input-group'>
      			<div class='input-group-addon'><span class='fa fa-user'></span></div>
      			<input class='form-control' id='first-name' name='first-name' placeholder='First Name' />
      		</div>
  		</div>
      	<div class='form-group'>
      		<div class='input-group'>
      			<div class='input-group-addon'><span class='fa fa-user'></span></div>
      			<input class='form-control' id='last-name' name='last-name' placeholder='Last Name' />
      		</div>
      	</div>
      	<div class='form-group'>
      		<div class='input-group'>
      			<div class='input-group-addon'>@</div>
      			<input class='form-control' id='email' name='email' placeholder='Email' />
      		</div>
      	</div>
      </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id='submit-new-user' class="btn btn-primary">Add User</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<div id='add-user-success-modal' class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content panel-success">
      <div class="modal-header panel-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Success!</h4>
      </div>
      <div class="modal-body">

      

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->