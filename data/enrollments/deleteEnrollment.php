<?php
/**
* This script will drop an enrollment from the database.
*/

//Getting the config
require_once '../../application/configuration.php';

$error = false;
$data = array();
$mapper = new EnrollmentMapper();

//The database id of the course
$id = isset($_POST['id']) ? $_POST['id'] : null;


startSession();
if(!validateCurrentUser()){
	$error = true;
	$data = "You must be logged in to do that!";
}

if(!$error){
	try{
		$mapper->delete($id);
	}
	catch(Exception $e){
		$error = true;
		$data = "Sorry, there was an issue deleting that enrollment";
	}
}

echo json_encode(array('error' => $error, 'data' => $data));
?>
