<?php
/**
* This script will drop an enrollment from the database.
*/

//Getting the config
require_once '../../application/configuration.php';

$error = false;
$data = array();
$mapper = new EnrollmentMapper();

//The database id of the course
$id = isset($_POST['id']) ? $_POST['id'] : null;
$is_new = isset($_POST['is_new']) ? $_POST['is_new'] : true;


startSession();
if(!validateCurrentUser()){
	$error = true;
	$data = "You must be logged in to do that!";
}

if(!$error){
	try{
		$enrollment = $mapper->find($id);
		$enrollment->setIsNew($is_new);
		$enrollment->save();
		$data = "Enrollment updated!";
	}
	catch(Exception $e){
		$error = true;
		$data = "Sorry, there was an issue setting the status for that enrollment";
	}
}

echo json_encode(array('error' => $error, 'data' => $data));
?>
